#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME) throw('USERNAME env var must be set');
if (!process.env.PASSWORD) throw('PASSWORD env var must be set');
if (!process.env.EMAIL) throw('EMAIL env var must be set');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const ADMIN_EMAIL = 'admin@cloudron.local';
    const ADMIN_PASSWORD = 'changeme';
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;
    const EMAIL = process.env.EMAIL;
    const SPACE_NAME = 'MySpace';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function adminLogin() {
        await browser.get(`https://${app.fqdn}/auth/login`);
        await waitForElement(By.id('authUsername'));
        await browser.findElement(By.id('authUsername')).sendKeys(ADMIN_EMAIL);
        await browser.findElement(By.id('authPassword')).sendKeys(ADMIN_PASSWORD);
        await browser.findElement(By.xpath('//div[@title="Sign In"]')).click();
        await waitForElement(By.id('profile-button'));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/auth/login`);
        await waitForElement(By.id('authUsername'));
        await browser.findElement(By.id('authUsername')).sendKeys(USERNAME);
        await browser.findElement(By.id('authPassword')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//div[@title="Sign In"]')).click();
        await waitForElement(By.id('profile-button'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.id('profile-button'));
        await browser.findElement(By.id('profile-button')).click();
        await waitForElement(By.xpath('//a[text()="Logout"]'));
        await browser.findElement(By.xpath('//a[text()="Logout"]')).click();
        await browser.sleep(5000);
        await waitForElement(By.id('authUsername'));
    }

    async function syncLDAPUsers() {
        await browser.get(`https://${app.fqdn}/settings/users`);
        await waitForElement(By.xpath('//div[text()="Sync with LDAP"]'));
        await browser.findElement(By.xpath('//div[text()="Sync with LDAP"]')).click();

        await waitForElement(By.xpath(`//div[text()="${EMAIL}"]`));
    }

    async function createSpace() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//div[text()="Space"]'));
        await browser.findElement(By.xpath('//div[text()="Space"]')).click();

        await waitForElement(By.id('new-space-name'));
        await browser.findElement(By.id('new-space-name')).sendKeys(SPACE_NAME);
        await waitForElement(By.xpath('//div[text()="Add"]'));
        await browser.findElement(By.xpath('//div[text()="Add"]')).click();

        await waitForElement(By.xpath(`//h1[text()="${SPACE_NAME}"]`));
    }

    async function spaceExists() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath(`//div[text()="${SPACE_NAME}"]`));
        await browser.findElement(By.xpath(`//div[text()="${SPACE_NAME}"]`)).click();
        await waitForElement(By.xpath(`//h1[text()="${SPACE_NAME}"]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can sync LDAP users', syncLDAPUsers);
    it('can logout', logout);
    it('can login', login);
    it('can create space', createSpace);
    it('space exists', spaceExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('space exists', spaceExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        browser.sleep(2000);
    });

    it('can login', login);
    it('space exists', spaceExists);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        browser.sleep(2000);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('space exists', spaceExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id com.documize.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can sync LDAP users', syncLDAPUsers);
    it('can logout', logout);
    it('can login', login);
    it('can create space', createSpace);
    it('space exists', spaceExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); browser.sleep(2000); });

    it('can login', login);
    it('space exists', spaceExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
