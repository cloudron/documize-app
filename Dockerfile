FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=documize/community versioning=semver extractVersion=^v(?<version>.+)$
ARG DOCUMIZE_VERSION=5.13.0

RUN curl -L https://github.com/documize/community/releases/download/v${DOCUMIZE_VERSION}/documize-community-linux-amd64 -o documize && \
    chmod +x /app/code/documize

COPY start.sh /app/code

CMD [ "/app/code/start.sh"]
