#!/bin/bash

set -eu

export PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD}
postgres_cli="psql -q -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE}"

if [[ ! -f /app/data/.salt ]]; then
  echo "Fresh installation, setting up data directory..."
  base64 /dev/urandom | head -c50 > /app/data/.salt
  echo "Done."
fi

salt=`cat /app/data/.salt`

export DOCUMIZEPORT=3000
export DOCUMIZEDBTYPE=postgresql
export DOCUMIZESALT=${salt}
export DOCUMIZELOCATION=selfhost
export DOCUMIZEDBTYPE=postgresql
export DOCUMIZEDB=host="${CLOUDRON_POSTGRESQL_HOST} port=${CLOUDRON_POSTGRESQL_PORT} dbname=${CLOUDRON_POSTGRESQL_DATABASE} user=${CLOUDRON_POSTGRESQL_USERNAME} password=${CLOUDRON_POSTGRESQL_PASSWORD} sslmode=disable"

configure() {
    echo "==> Configure SMTP"
    senderName="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Documize}"
    SMTP_CONFIG_JSON="{\"host\":\"${CLOUDRON_MAIL_SMTP_SERVER}\",\"password\":\"${CLOUDRON_MAIL_SMTP_PASSWORD}\",\"port\":\"${CLOUDRON_MAIL_STARTTLS_PORT}\",\"sender\":\"${CLOUDRON_MAIL_FROM}\",\"userid\":\"${CLOUDRON_MAIL_SMTP_USERNAME}\",\"senderName\":\"${senderName}\",\"fqdn\":\"\",\"usessl\":false}"
    ${postgres_cli} -n -o /dev/null -c "INSERT INTO dmz_config (c_key, c_config) VALUES ('SMTP', '${SMTP_CONFIG_JSON}') ON CONFLICT (c_key) DO UPDATE SET c_config = '${SMTP_CONFIG_JSON}';"

    echo "==> Configure LDAP"
    LDAP_CONFIG_JSON="{\"serverHost\":\"${CLOUDRON_LDAP_HOST}\",\"serverPort\":${CLOUDRON_LDAP_PORT},\"bindDN\":\"${CLOUDRON_LDAP_BIND_DN}\",\"bindPassword\":\"${CLOUDRON_LDAP_BIND_PASSWORD}\",\"userFilter\":\"(&(objectclass=user))\",\"attributeUserRDN\":\"username\",\"attributeUserFirstname\":\"givenName\",\"attributeUserLastname\":\"sn\",\"attributeUserEmail\":\"mail\",\"attributeGroupMember\":\"memberof\",\"defaultPermissionAddSpace\":true,\"allowFormsAuth\":true,\"groupFilter\":\"(&(objectclass=group))\",\"baseDN\":\"${CLOUDRON_LDAP_USERS_BASE_DN}\"}"
    ${postgres_cli} -n -o /dev/null -c "UPDATE dmz_org SET c_authconfig='${LDAP_CONFIG_JSON}',c_authprovider='ldap' WHERE id=1;"

    echo "==> done"
}

setup() {
    while ! curl -s http://localhost:3000 -o /dev/null; do
        echo "==> Waiting for documize to start"
        sleep 1
    done

    # pre-setup admin credentials
    email="admin%40cloudron.local"
    firstname="Local"
    lastname="Admin"
    password="changeme"

    # first we need to extract some dbhash as the setup password of sorts https://github.com/documize/community/blob/adb7b4d7bf3e9e3f42aa23821db689686ed4b526/core/database/setup_endpoint.go#L65
    echo "==> Setting up admin account"
    db_hash=`curl http://localhost:3000/setup | grep 'property="dbhash"' | sed "s/.*content=\"\(.*\)\".*/\1/"`
    curl --request POST -o /dev/null "http://localhost:3000/api/setup?dbname=${CLOUDRON_POSTGRESQL_DATABASE}&dbhash=${db_hash}&title=Cloudron&message=Documize+Community+instance+on+Cloudron&allowAnonymousAccess=false&firstname=${firstname}&lastname=${lastname}&email=${email}&password=${password}&activationKey=&edition=Community"

    configure
}

if ! ${postgres_cli} -n -c "SELECT * FROM dmz_config;" -o /dev/null; then
    setup &
else
    configure
fi

chown -R cloudron:cloudron /app/data

echo "=> Starting server"
cd /app/data # backups are stored in the same path (https://github.com/documize/community/blob/master/domain/backup/backup.go#L82
exec gosu cloudron:cloudron /app/code/documize -logtostderr
