This app is pre-setup with an admin account. The initial credentials are:

**Email**:    admin@cloudron.local<br/>
**Password**: changeme<br/>

**LDAP must by synced manually by the admin in the User Management page for Cloudron users to login.**
