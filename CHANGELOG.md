[0.1.0]
* Initial commit

[0.2.0]
* Set forum url

[0.3.0]
* Update Documize to 5.2.2

[0.4.0]
* Update Documize to 5.3.0

[0.5.0]
* Update Documize to 5.4.0

[0.6.0]
* Update Documize to 5.4.1
* [Full changelog](https://github.com/documize/community/releases/tag/v5.4.1)
* Fix robots.txt and sitemap.xml regression
* Fix middleware for favicon
* Set TLS minimum to 1.3

[0.7.0]
* Update Documize to 5.5.0
* [Full changelog](https://github.com/documize/community/releases/tag/v5.5.0)
* French translation
* Enhanced user permission checks
* Upgrade to Go v1.20.0

[1.0.0]
* First stable release with 5.5.0
* Pre-setup admin
* Configured SMTP
* Enable LDAP user integration

[1.1.0]
* Update base image to 4.0.0
* Email display name support

[1.1.1]
* Fix backups (import/export)

[1.2.0]
* Update Documize to 5.6.0
* [Full changelog](https://github.com/documize/community/releases/tag/v5.6.0)
* New Japanese translation
* French translation updates

[1.3.0]
* Update Documize to 5.7.0
* [Full changelog](https://github.com/documize/community/releases/tag/v5.7.0)
* Updated Japanese translation
* SQL layer improvements
* Upgrade Go to v1.20.4

[1.4.0]
* Update Documize to 5.8.0
* [Full changelog](https://github.com/documize/community/releases/tag/v5.8.0)
* Translation updates
* Fixed version selection bugs

[1.4.1]
* Update Documize to 5.9.0
* [Full changelog](https://github.com/documize/community/releases/tag/v5.9.0)
* Improvements to approvals process
* Various bug fixes
* Bump Go to v1.21.0

[1.5.0]
* Update Documize to 5.10.0
* [Full changelog](https://github.com/documize/community/releases/tag/v5.10.0)
* Improvements to SQL Server database performance
* Improvements to Italian language support
* Bump Go to v1.21.1

[1.6.0]
* Update base image to 4.2.0

[1.7.0]
* Update Documize to 5.11.1
* [Full changelog](https://github.com/documize/community/releases/tag/v5.11.1)
* Improved database connectivity and LDAP/CAS authenticator providers
* Bumped Go to v1.21.5

[1.7.1]
* Update Documize to 5.11.2
* [Full changelog](https://github.com/documize/community/releases/tag/v5.11.2)
* Implement Microsoft SQL Azure v12+ database checks

[1.7.2]
* Update Documize to 5.11.3
* [Full changelog](https://github.com/documize/community/releases/tag/v5.11.3)

[1.8.0]
* Update Documize to 5.12.0
* [Full changelog](https://github.com/documize/community/releases/tag/v5.12.0)

[1.9.0]
* Update community to 5.13.0
* [Full Changelog](https://github.com/documize/community/releases/tag/v5.13.0)
* Added Spanish language support
* Bumped Go to v1.23.4

[1.10.0]
* checklist added to manifest

